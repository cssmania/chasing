var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync').create();
var header = require('gulp-header');
var cleanCSS = require('gulp-clean-css');
var rename = require("gulp-rename");
var uglify = require('gulp-uglify');
var filter = require('gulp-filter');
var pkg = require('./package.json');
const autoprefixer = require('gulp-autoprefixer');
var plumber = require('gulp-plumber');


gulp.task('sass', function () {
  return gulp.src('sass/*.scss')
    .pipe(sass())
    .pipe(plumber())    
    .pipe(autoprefixer())
    .pipe(gulp.dest('css'))
    .pipe(browserSync.reload({
      stream: true
    }))     
});
 



// Run everything

// Configure the browserSync task
gulp.task('browserSync', function() {
    browserSync.init({
        server: {
            baseDir: 'D:\/wamp\/www\/chasing'
        },
    })
})

// Dev task with browserSync
gulp.task('dev', ['browserSync','sass'], function() {
    gulp.watch('sass/*.scss', ['sass']);
    // Reloads the browser whenever HTML or JS files change
    gulp.watch('*.html', browserSync.reload);
    gulp.watch('js/**/*.js', browserSync.reload);
});
